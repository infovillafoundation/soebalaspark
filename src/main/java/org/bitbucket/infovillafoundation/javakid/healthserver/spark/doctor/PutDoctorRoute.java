package org.bitbucket.infovillafoundation.javakid.healthserver.spark.doctor;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Doctor;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Gender;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PutDoctorRoute extends JsonTransformer {

    public PutDoctorRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        long doctorId = Long.parseLong(request.params(":id"));
        Doctor doctor = Ebean.find(Doctor.class, doctorId);

        if (doctor != null) {
            String newName = request.queryParams("name");
            if (newName != null) {
                doctor.setName(newName);
            }

            String newGender = request.queryParams("gender");
            if (newGender != null) {
                if (newGender.equals("male")) {
                    doctor.setGender(Gender.Male);
                }
                else {
                    doctor.setGender(Gender.Female);
                }
            }

            String newEmail = request.queryParams("email");
            if (newEmail != null) {
                doctor.setEmail(newEmail);
            }

            String newDob = request.queryParams("dob");
            if (newDob != null) {
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date newDobUpdate = null;
                try {
                    newDobUpdate = format.parse(newDob);
                } catch (ParseException e) {

                }
                doctor.setDateOfBirth(newDobUpdate);
            }

            String newPhoneNumber = request.queryParams("phonenum");
            if (newPhoneNumber != null) {
                doctor.setPhoneNumber(newPhoneNumber);
            }

            String newLicenseNumber = request.queryParams("license");
            if (newLicenseNumber != null) {
                doctor.setLicenseNumber(newLicenseNumber);
            }

            String newSpecialisation = request.queryParams("specialisation");
            if (newSpecialisation != null) {
                doctor.setSpecialisation(newSpecialisation);
            }

            Ebean.update(doctor);
            response.status(204); // 204 No Content
            return "";
        } else {
            response.status(404); // 404 Not found
            return createErrorResponse("Not found");
        }
    }
    
}
