package org.bitbucket.infovillafoundation.javakid.healthserver.spark.employee;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Employee;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Gender;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PutEmployeeRoute extends JsonTransformer {

    public PutEmployeeRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        long employeeId = Long.parseLong(request.params(":id"));
        Employee employee = Ebean.find(Employee.class, employeeId);

        if (employee != null) {
            String newName = request.queryParams("name");
            if (newName != null) {
                employee.setName(newName);
            }

            String newGender = request.queryParams("gender");
            if (newGender != null) {
                if (newGender.equals("male")) {
                    employee.setGender(Gender.Male);
                }
                else {
                    employee.setGender(Gender.Female);
                }
            }

            String newSalary = request.queryParams("salary");
            Double newSalarydouble = Double.parseDouble(newSalary);
            if (newSalarydouble != null) {
                employee.setSalary(newSalarydouble);
            }

            String newhireDay = request.queryParams("hireDay");
            if (newhireDay != null) {
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date newhireDayUpdate = null;
                try {
                    newhireDayUpdate = format.parse(newhireDay);
                } catch (ParseException e) {

                }
                employee.setHireDay(newhireDayUpdate);
            }


            Ebean.update(employee);
            response.status(204); // 204 No Content
            return "";
        } else {
            response.status(404); // 404 Not found
            return createErrorResponse("Not found");
        }
    }
    
}
