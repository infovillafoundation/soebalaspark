package org.bitbucket.infovillafoundation.javakid.healthserver.spark.patient;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

public class GetPatientRoute extends JsonTransformer {

    public GetPatientRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        long patientId = Long.parseLong(request.params(":id"));
        Patient patient = Ebean.find(Patient.class, patientId);
        if (patient != null) {
            return patient;
        } else {
            response.status(404); // 404 Not found
            return createErrorResponse("Not found");
        }
    }
}
