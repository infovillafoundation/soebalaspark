package org.bitbucket.infovillafoundation.javakid.healthserver.domain;

import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Gender;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by Way Yan on 12/11/2015.
 */

@Entity
public class Doctor {
    @Id // Does it work?
    private int id;
    @Column(length = 120, nullable = false, unique = false)
    private String name;
    @Column(nullable = false, unique = false)
    private Gender gender;
    @Column(length = 120, nullable = true, unique = true)
    private String email;
    @Column(nullable = false, unique = false)
    private Date dateOfBirth;
    @Column(length = 11, nullable = false, unique = true)
    private String phoneNumber;
    @Column(length = 10, nullable = false, unique = true)
    private String licenseNumber;
    @Column(length = 20, nullable = false, unique = false)
    private String specialisation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }
}
