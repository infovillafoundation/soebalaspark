package org.bitbucket.infovillafoundation.javakid.healthserver.spark.employee;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Employee;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

public class GetEmployeesRoute extends JsonTransformer {

    public GetEmployeesRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        long employeeId = Long.parseLong(request.params(":id"));
        Employee employee = Ebean.find(Employee.class, employeeId);
        if (employee != null) {
            return employee;
        } else {
            response.status(404); // 404 Not found
            return createErrorResponse("Not found");
        }
    }
}
