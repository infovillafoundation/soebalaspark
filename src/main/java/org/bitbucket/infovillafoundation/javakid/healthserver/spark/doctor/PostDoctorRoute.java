package org.bitbucket.infovillafoundation.javakid.healthserver.spark.doctor;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Doctor;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

import java.io.IOException;

public class PostDoctorRoute extends JsonTransformer {
    public PostDoctorRoute(String path) {
        super(path);
    }



    @Override
    public Object handle(Request request, Response response) {
        Doctor doctor = null;
		try {
            doctor = mapper.readValue(request.body(), Doctor.class);
		} catch (IOException e) {
			
			response.status(500);
			
			return createErrorResponse("Doctor couldn't be saved");
		}
        Ebean.save(doctor);
        response.status(201); // 201 Created
        return doctor;
    }
    
}
