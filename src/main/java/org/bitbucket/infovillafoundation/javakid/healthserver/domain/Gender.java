package org.bitbucket.infovillafoundation.javakid.healthserver.domain;

/**
 * Created by Way Yan on 12/9/2015.
 */

public enum Gender {
    Male, Female;
}
