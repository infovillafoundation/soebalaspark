package org.bitbucket.infovillafoundation.javakid.healthserver.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.GregorianCalendar;

@Entity
public class Employee {
    @Id
    private int id;
    @Column(length = 120, nullable = false, unique = false)
    private String name;
    @Column(nullable = false, unique = false)
    private Gender gender;
    @Column(length = 8, nullable = true, unique = true)
    private double salary;
    @Column(nullable = false, unique = false)
    private Date hireDay;

    public Employee() {

    }

  public Employee(String n, Gender g, double s, int year, int month, int day) {
    name = n;
    salary = s;
      gender = g;
    GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
    // GregorianCalendar uses 0 for January
    hireDay = calendar.getTime();
  }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
    return name;
  }

  public double getSalary() {
    return salary;
  }

  public Date getHireDay() {
    return hireDay;
  }

    public Gender getGender() {
        return gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void setHireDay(Date hireDay) {
        this.hireDay = hireDay;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void raiseSalary(double byPercent) {
    double raise = salary * byPercent / 100;
    salary += raise;
  }
}