package org.bitbucket.infovillafoundation.javakid.healthserver.spark.employee;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Employee;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

import java.io.IOException;

public class PostEmployeeRoute extends JsonTransformer {
    public PostEmployeeRoute(String path) {
        super(path);
    }



    @Override
    public Object handle(Request request, Response response) {
        Employee employee = null;
		try {
            employee = mapper.readValue(request.body(), Employee.class);
		} catch (IOException e) {
			
			response.status(500);
			
			return createErrorResponse("Employee couldn't be saved");
		}
        Ebean.save(employee);
        response.status(201); // 201 Created
        return employee;
    }
    
}
