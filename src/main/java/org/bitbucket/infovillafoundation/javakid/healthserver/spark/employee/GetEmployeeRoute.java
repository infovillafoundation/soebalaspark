package org.bitbucket.infovillafoundation.javakid.healthserver.spark.employee;

import com.avaje.ebean.Ebean;
import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Employee;
import org.bitbucket.infovillafoundation.javakid.healthserver.spark.JsonTransformer;
import spark.Request;
import spark.Response;

public class GetEmployeeRoute extends JsonTransformer {

    public GetEmployeeRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        System.out.println(Ebean.find(Employee.class).findList().size());
        return Ebean.find(Employee.class).findList();
    }
    
}
