package org.bitbucket.infovillafoundation.javakid.healthserver;

import com.avaje.ebean.Ebean;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.bitbucket.infovillafoundation.javakid.healthserver.domain.Patient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BookTest extends BaseEbeanTest {

    @Before
    public void importData() throws URISyntaxException, IOException {
        importData("testdata/data.sql");
    }

    @Test
    public void firstPageTest() {
        List<Patient> patients = Ebean.find(Patient.class).findList();
        Assert.assertEquals(3, patients.size());
    }

}
